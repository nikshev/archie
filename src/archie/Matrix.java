package archie;

import java.util.*;

public class Matrix {
   private Vector <Integer> main;
   private ArrayList2d <Integer> incidence_matrix;
   private ArrayList2d <Integer> incidence_matrix_copy;
   private ArrayList2d <Integer> vertex;
   Vector <Integer> regenerated;
   private int last_vertex=-1;
       
  /**
   * Matrix constructor
   */
  public Matrix(){
	main=new Vector <Integer>();  
	incidence_matrix=new ArrayList2d<Integer>();
	incidence_matrix_copy=new ArrayList2d<Integer>();
	vertex=new ArrayList2d<Integer>();
	regenerated=new Vector <Integer>();
  }
  
 
  /**
   * Add new element to matrix object
   */
  public void addElement(int element){
	  try {
	   this.main.add(element);	  
	   int vertex_to=this.getVertex(element);
	   if (this.getVertexCount()!=0&&last_vertex!=-1){
 	 	   int vertex_from=last_vertex;
 	 	   vertex_to=this.inesertIntoMatrix(vertex_from,vertex_to);
	   } 
	   this.last_vertex=vertex_to;
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.addElement; Exception:"+ex.getMessage());
	  }
  }
  
  /**
   * Return string representation for incidence matrix
   * @return
   */
  public String  incidenceMatrixToString(){
	  String local_result="{";
	  try {
	   for (int i=0;i<this.getIncidenceMatrixRowCount();i++){
		 local_result+="{";  
		 for (int j=0; j<this.getIncidenceMatrixColCount(i);j++){
			 if (this.getIncidenceMatrixColCount(i)-j>1)
			     local_result+=this.incidence_matrix.get(i, j).toString()+"; ";
			 else
				 local_result+=this.incidence_matrix.get(i, j).toString();
		 }
		 local_result+="}";
	   }
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.incidenceMatrixToString; Exception:"+ex.getMessage());
  	  }
	  local_result+="}";
	return local_result;
  }
  
  /**
   * Return string representation of vertex vector
   * @return
   */
  public String vertexToString(){
	  String local_result="{";
	  try {
	   for (int i=0;i<this.getVertexCount();i++){
		  local_result+="{";  
	      local_result+=i+"; ";
          local_result+=this.vertex.get(i, 1).toString();
		  local_result+="}";
	   }
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.vertexToString; Exception:"+ex.getMessage());
  	  }
	  local_result+="}";
	return local_result;
  }
  
   
  /**
   * Return vertex number if value in vertex vector or add new vertex
   * @param element
   * @return
   */
  private int getVertex(int element){
	int local_vertex=-1;
	int local_vertex_count=-1;
	try {
	 local_vertex_count=this.getVertexCount();
	 if (local_vertex_count>0) {
 		 for (int i=0;i<local_vertex_count;i++){
		 	 if (this.vertex.get(i, 1)==element)
			 	 local_vertex=this.vertex.get(i, 0);
		 }
		 if (local_vertex==-1)
			 local_vertex=this.addVertex(element);
	 } else {
	 	 local_vertex=this.addVertex(element);
	 }
	} catch (Exception ex){
	  System.out.println("Exception in Matrix.getVertex; Exception:"+ex.getMessage());
	}
	return local_vertex;
  }
  
  /**
   * Add new vertex 
   * @param vertex
   */
  private int addVertex(int element){
	  int local_vertex_count=-1;  
	  try {  
	   local_vertex_count=this.getVertexCount();
	   this.vertex.Add(local_vertex_count, local_vertex_count); //Add vertex №
       this.vertex.Add(element, local_vertex_count); //Add vertex value
       for (int i=0;i<local_vertex_count;i++)
    	 this.incidence_matrix.Add(0, i); //Add column in other rows
       for (int i=0;i<local_vertex_count+1;i++)
        this.incidence_matrix.Add(0, local_vertex_count); //Add columns in new row*/
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.addVertex; Exception:"+ex.getMessage());
	  }
	return local_vertex_count;
  }
  
  /**
   * Return vertex count
   * @return
   */
  private int getVertexCount(){
	return this.vertex.getNumRows();
  }
  
  /**
   * Return incidence matrix row count
   * @return
   */
  private int getIncidenceMatrixRowCount(){
	return this.incidence_matrix.getNumRows();
  }
  
  /**
   * Return incidence matrix column count()
   * @return
   */
  private int getIncidenceMatrixColCount(int row){
	return this.incidence_matrix.getNumCols(row);
  }
  
  /**
   * Insert into matrix new path
   * @param vertex_from
   * @param vertex_to
   */
  private int inesertIntoMatrix(int vertex_from,int vertex_to){
	  int new_vertex_from=-1;
	  int new_vertex_to=-1;
	  try {
		//System.out.println("vertex_from="+vertex_from+"; vertex_to="+vertex_to);
 	    if (this.vertex.get(vertex_to, 1)==this.vertex.get(vertex_from, 1)){
			 // int new_vertex_from=this.addVertex(this.vertex.get(vertex_from, 1));
			  new_vertex_to=this.addVertex(this.vertex.get(vertex_to, 1));	
			  this.incidence_matrix.set(vertex_from, new_vertex_to, 1);
			//  System.out.println("new_vertex_to="+new_vertex_to);
			} else 
		 if (this.doublePath(vertex_from, vertex_to)){
			 new_vertex_from=this.addVertex(this.vertex.get(vertex_from, 1));
			 new_vertex_to=this.addVertex(this.vertex.get(vertex_to, 1));
	         this.incidence_matrix.set(new_vertex_from,new_vertex_to,1);
	       //  System.out.println("new_vertex_from="+new_vertex_from+"; new_vertex_to="+new_vertex_to);
		 } 
		 else {
           this.incidence_matrix.set(vertex_from, vertex_to, 1);
           new_vertex_to=vertex_to;
		 }
		  
		//}
        //System.out.println("im:"+this.incidenceMatrixToString()+"\r\n");
	   } catch (Exception ex){
		  System.out.println("Exception in Matrix.inesertIntoMatrix; Exception:"+ex.getMessage());
	  }
	  return new_vertex_to;
  }
  
  /**
   * Return regenerated data
   * @return
   */
 private void regenerateData(){
	  try {
		regenerated.clear();
		this.incidence_matrix_copy=this.incidence_matrix;
		this.regenerated.add(this.vertex.get(0, 1));
		this.findPath(0,this.getIncidenceMatrixRowCount());
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.regenerateData; Exception:"+ex.getMessage());
	  }
  }
  
  private void findPath(int from_vertex,int to_vertex){
	  try {
		  //System.out.println("from_vertex:"+from_vertex);
		  //System.out.println("to_vertex:"+to_vertex);
	  for (int i=from_vertex;i<to_vertex;i++){
			for (int j=0;j<this.getIncidenceMatrixColCount(i);j++){
			  //System.out.println("i="+i+" j="+j);	
			  //System.out.println("im_v:"+this.incidence_matrix.get(i, j));	
              if (this.incidence_matrix_copy.get(i, j)==1){
            	 // System.out.println("v_v:"+this.vertex.get(j, 1));
            	  this.regenerated.add(this.vertex.get(j, 1));
              	  this.incidence_matrix_copy.set(i, j,0);
              	  //System.out.println("Regenerated:"+this.regenerated.toString());
              	  //System.out.println("--------------------------------------------\r\n");
             	  this.findPath(j,to_vertex);
              }
			}
		}
	  } catch (Exception ex){
		  System.out.println("Exception in Matrix.findPath; Exception:"+ex.getMessage());
	  }
  }
  
  public Vector <Integer> getRegeneratedData(){
	  this.regenerateData();
	   return regenerated;
  }
  
  private boolean doublePath(int vertex_from,int vertex_to){
	if (this.incidence_matrix.get(vertex_from, vertex_to)==1 || 
		this.incidence_matrix.get(vertex_to,vertex_from)==1)  
	 return true;
	else
	 return false;		
  }
  
}
